csound-manual (1:7.00.0+dfsg-1) unstable; urgency=medium

  * New upstream version 7.00.0+dfsg
    + Omit the dfsg count
  * Bump Standards-Version to 4.7.0
  * Remove obsolete dh-buildinfo

 -- Dennis Braun <snd@debian.org>  Sun, 09 Feb 2025 15:10:20 +0100

csound-manual (1:6.18.0+dfsg0-1) unstable; urgency=medium

  * New upstream version 6.18.0+dfsg0
  * Refresh patchset
  * Bump Standards Version to 4.6.1
  * Bump my d/copyright year
  * d/watch: Fix github regex, and use +dfsg tag instead of ~dfsg
  * Add salsa ci config
  * Change my email adress to snd@debian.org

 -- Dennis Braun <snd@debian.org>  Mon, 28 Nov 2022 22:00:02 +0100

csound-manual (1:6.16.0~dfsg-1) unstable; urgency=medium

  [ Felipe Sateler ]
  * Remove myself from uploaders

  [ Dennis Braun ]
  * New upstream version 6.16.0~dfsg
  * d/control: Bump S-V to 4.6.0
  * Refresh patchset
  * d/copyright: Update year for my entry

  [ Jenkins ]
  * Refer to specific version of license LGPL-2.1+

 -- Dennis Braun <d_braun@kabelmail.de>  Mon, 27 Sep 2021 16:34:46 +0200

csound-manual (1:6.15.0~dfsg-1) unstable; urgency=medium

  * New upstream version 6.15.0~dfsg
  * Update patchset
  * Update d/copyright dirs
  * Bump dh-compat to 13
  * Bump S-V to 4.5.1
  * Add d/upstream/metadata
  * Remove README.source, infos obsolete

 -- Dennis Braun <d_braun@kabelmail.de>  Sun, 07 Feb 2021 15:12:47 +0100

csound-manual (1:6.14.0~dfsg-1) unstable; urgency=medium

  * New upstream version 6.14.0~dfsg
  * d/control:
    + Bump debhelper-compat to 12
    + Bump standards version to 4.5.0
    + csound-doc: Add Multi-Arch: foreign
    + Build with python 3 (Closes: #936348, #942921)
    + Add me as uploader
    + Update homepage
  * d/copyright:
    + Update source URL
    + Add myself to the d/ section
  * d/patches:
    + Drop Fix-FTBFS-due-to-newer-python-pygments.patch
    + Add Build_with_python3.patch
  * d/rules: Use the https protocol

 -- Dennis Braun <d_braun@kabelmail.de>  Sun, 05 Apr 2020 11:11:55 -0400

csound-manual (1:6.12.0~dfsg-2) unstable; urgency=medium

  * Fix FTBFS due to newer python-pygments.
    Score statements are now a 2-tuple instead of a 3-tuple (Closes: #918560)

 -- Felipe Sateler <fsateler@debian.org>  Fri, 11 Jan 2019 20:08:47 -0300

csound-manual (1:6.12.0~dfsg-1) unstable; urgency=medium

  * New upstream version 6.12.0~dfsg

 -- Felipe Sateler <fsateler@debian.org>  Wed, 21 Nov 2018 22:51:57 -0300

csound-manual (1:6.11.0~dfsg-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org
  * New upstream version 6.11.0~dfsg
  * Refresh patches
  * Add Rules-Requires-Root: no. We don't need root for building
  * Drop unused build-dependency devscripts
  * Bump debhelper compat to 11.
    Also switch to depending on debhelper-compat virtual package
  * Bump Standards-Version to 4.2.1 (no changes)
  * Remove unmatched entries in d/copyright

 -- Felipe Sateler <fsateler@debian.org>  Sun, 16 Sep 2018 10:54:11 -0300

csound-manual (1:6.09.0~dfsg-2) unstable; urgency=medium

  * Add missing build-dependencies (Closes: #871814)

 -- Felipe Sateler <fsateler@debian.org>  Fri, 11 Aug 2017 19:58:07 -0400

csound-manual (1:6.09.0~dfsg-1) unstable; urgency=medium

  * New upstream version 6.09.0~dfsg
    - Refresh patches
  * Bump standards version
  * Bump debhelper compat to 10

 -- Felipe Sateler <fsateler@debian.org>  Thu, 10 Aug 2017 19:59:50 -0400

csound-manual (1:6.08.0~dfsg-1) unstable; urgency=medium

  * New upstream release
    - Remove deleted files from d/copyright
    - Refresh patches

 -- Felipe Sateler <fsateler@debian.org>  Mon, 12 Dec 2016 16:07:42 -0300

csound-manual (1:6.07.0~dfsg-1) unstable; urgency=medium

  * Imported Upstream version 6.07.0~dfsg
    - Refresh patches
    - Remove more python opcode references
  * Move Vcs-* urls to pkg-multimedia
  * Update copright format
  * Drop CDBS upstream handling in favor of uscan
  * Drop overrides accidentally copied from csound package
  * Bump debhelper compat level
  * Switch from CDBS to dh sequencer
  * Use gbp pq for patches
  * Do not install second copy of copyright file
  * Drop Jonas from uploaders. Thanks for all the work!

 -- Felipe Sateler <fsateler@debian.org>  Sun, 17 Jul 2016 11:38:28 -0400

csound-manual (1:6.02~dfsg-2) unstable; urgency=medium

  * New upstream version
    - Refresh patch
  * Remove manpages, no longer built upstream

 -- Felipe Sateler <fsateler@debian.org>  Fri, 31 Jan 2014 11:39:23 -0300

csound-manual (1:5.13~dfsg-1) unstable; urgency=low

  * New upstream release
   - Add Menno Knevel to the copyright file
   - Refresh patch 2000-stripped-opcodes.diff
   - Update copyright hints file
  * Update Vcs-* headers
  * Bump Standards-Version

 -- Felipe Sateler <fsateler@debian.org>  Mon, 01 Aug 2011 22:21:58 -0400

csound-manual (1:5.12~dfsg2-1) unstable; urgency=low

  [ Felipe Sateler ]
  * New upstream release
    - Refresh patches
  * Fix override disparity: this package is optional, not extra
  * Generalize spurious version removal in csound.1
  * Completely filter out the python opcodes, update copyright file
    accordingly
  * Update copyright years for the manual
  * Update copyright years on the debian subdir
  * Update copyright_hints file, no new licenses

  [ Jonas Smedegaard ]
  * Stop including local CDBS snippets or declare DEB_MAINTAINER_MODE:
    adopted in main cdbs now.
  * Declare package relations recursively expanded.
  * Refer to FSF website (not postal address) in rules file header, and
    bump copyright year.
  * Bump policy compliance to Standards Version 3.8.4.
  * Refresh patch 2000 with compacting quilt options --no-index --no-
    timestamps -pab.
  * Add DEP-3 header to patch 2000.
  * Fix update repackaging hints to use new
    DEB_UPSTREAM_REPACKAGE_EXCLUDES with ./ appended to each file path.
  * Update copyright_hints to newer format (to easier see actual
    licensing changes in recent upstream release.
  * Update copyright file to DEP5 draft 135 format; drop files section
    for no longer shipped CDBS snippets; bump a coyright year.
  * Use source format 3.0 (quilt), and stop including patchsys-quilt.mk.

 -- Felipe Sateler <fsateler@gmail.com>  Thu, 10 Jun 2010 16:27:37 -0400

csound-manual (1:5.10~dfsg-2) unstable; urgency=low

  * Rewrite copyright to use DEP5 r54 proposed machine-readable format.
    Mention files excluded from our redistributed source.
  * Strip bogus comment in changelog entry for 1:5.10~dfsg-1 about
    bad copyright/licensing:  The problem was fixed prior to release.
  * Update CDBS snippets:
    + Fix package-relations cleanup when using debhelper 7
    + Implement fail-source-not-repackaged rule in upstream-tarball.mk
  * Use new fail-source-not-repackaged rule.
  * Fix double inclusion of copyright-check.mk.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 05 Jun 2009 11:22:55 +0200

csound-manual (1:5.10~dfsg-1) unstable; urgency=low

  [ Felipe Sateler ]
  * Adopt csound-doc. Closes: #473003
  * Split manual from csound source.
  * Note the new maintainers and git repositories.
  * Use epoch to allow upgrades from previous versions.
  * Do proper copyright notice.

  [ Jonas Smedegaard ]
  * Repackage to use CDBS.
  * Add README.source documenting use of Git and CDBS.
  * Rewrite debian/copyright to use new proposed format.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 29 May 2009 10:10:56 +0200
